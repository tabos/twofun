<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright 2019-2024 Jan-Michael Brummer <jan.brummer@tabos.org>
-->

<component type="desktop-application">
  <id>org.tabos.twofun</id>
  <launchable type="desktop-id">org.tabos.twofun.desktop</launchable>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0</project_license>
  <name>TwoFun</name>
  <summary>Touch based reaction game for two players</summary>
  <description>
    <p>Multiplayer game collection for touch devices, like "Hit when white", "3 of a kind", "Color matching", and more</p>
  </description>
  <developer id="org.tabos">
    <name>Jan-Michael Brummer</name>
  </developer>
  <url type="homepage">https://tabos.org/projects/twofun/</url>
  <url type="bugtracker">https://gitlab.com/tabos/twofun/issues</url>
  <url type="vcs-browser">https://gitlab.com/tabos/twofun/</url>
  <url type="donation">https://www.paypal.me/tabos/10</url>
  <recommends>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </recommends>
  <requires>
    <display_length compare="ge">360</display_length>
  </requires>
  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.com/tabos/twofun/-/raw/main/data/screenshots/twofun1.png</image>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/tabos/twofun/-/raw/main/data/screenshots/twofun2.png</image>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/tabos/twofun/-/raw/main/data/screenshots/twofun3.png</image>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/tabos/twofun/-/raw/main/data/screenshots/twofun4.png</image>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/tabos/twofun/-/raw/main/data/screenshots/twofun5.png</image>
    </screenshot>
  </screenshots>
  <update_contact>jan.brummer_AT_tabos.org</update_contact>
  <translation type="gettext">twofun</translation>
  <content_rating type="oars-1.1" />
  <releases>
    <release version="0.5.1" date="2024-11-23" />
    <release version="0.5.0" date="2024-11-11" />
    <release version="0.4.1" date="2021-10-01" />
    <release version="0.4.0" date="2021-09-19" />
    <release version="0.3.2" date="2019-10-18" />
    <release version="0.3.1" date="2019-10-13" />
    <release version="0.3.0" date="2019-10-04" />
    <release version="0.2.2" date="2019-09-30" />
    <release version="0.2.1" date="2019-08-31" />
  </releases>
  <custom>
    <value key="Purism::form_factor">mobile</value>
  </custom>
</component>
