/* Copyright 2018-2021 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>
#include <gio/gio.h>

#include <adwaita.h>

#include "game.h"

typedef struct {
  gboolean progress_active;
} TwoFunGamePrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (TwoFunGame, twofun_game, GTK_TYPE_BOX)

enum {
  PROGRESS_ACTIVATED,
  LAST_SIGNAL
};

static guint signals [LAST_SIGNAL] = { 0 };

static void
twofun_game_class_init (TwoFunGameClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  signals[PROGRESS_ACTIVATED] = g_signal_new ("progress-activated",
                                              G_TYPE_FROM_CLASS (object_class),
                                              G_SIGNAL_RUN_LAST,
                                              0, NULL, NULL, NULL,
                                              G_TYPE_NONE, 1,
                                              G_TYPE_DOUBLE);
}

static void
twofun_game_init (TwoFunGame *self)
{
}

gchar *
twofun_game_get_description (TwoFunGame *game)
{
  TwoFunGameClass *class = TWOFUN_GAME_GET_CLASS (game);

  if (class->get_description)
    return class->get_description (game);

  return NULL;
}

void
twofun_game_next_step (TwoFunGame *game)
{
  TwoFunGameClass *class = TWOFUN_GAME_GET_CLASS (game);

  if (class->next_step)
    class->next_step (game);
}

gboolean
twofun_game_is_valid (TwoFunGame *game)
{
  TwoFunGameClass *class = TWOFUN_GAME_GET_CLASS (game);

  if (class->is_valid)
    return class->is_valid (game);

  return TRUE;
}

gdouble
twofun_game_get_game_time (TwoFunGame *game)
{
  TwoFunGameClass *class = TWOFUN_GAME_GET_CLASS (game);

  if (class->get_game_time)
    return class->get_game_time (game);

  return 1.0f;
}
