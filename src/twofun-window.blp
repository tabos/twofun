using Gtk 4.0;
using Adw 1;

menu primary_menu {
  section {
    item {
      label: _("New Game");
      action: "app.start";
    }

    item {
      label: _("Preferences");
      action: "app.preferences";
    }
  }

  section {
    item {
      label: _("About TwoFun");
      action: "app.about";
    }
  }
}

template $TwoFunWindow: Adw.ApplicationWindow {
  can-focus: false;
  default-width: 360;
  default-height: 648;

  WindowHandle {
    Box {
      orientation: vertical;

      Adw.HeaderBar {
        show-end-title-buttons: true;

        title-widget: Adw.WindowTitle {
          title: "TwoFun";
        };

        [end]
        MenuButton {
          focus-on-click: false;
          menu-model: primary_menu;
          direction: none;
        }

        styles [
          "flat",
          "titlebar",
        ]
      }

      Button player_one_button {
        clicked => $player_one_button_clicked_cb();
        margin-start: 6;
        margin-end: 6;
        margin-top: 6;

        Box {
          orientation: vertical;
          hexpand: true;
          vexpand: true;
          margin-start: 12;
          margin-end: 12;
          margin-top: 12;
          margin-bottom: 12;

          Label player_one_description {}

          Label {
            label: _("Player 1");
            wrap: true;
            vexpand: true;
            valign: end;
          }

          styles [
            "rotate180",
          ]
        }
      }

      Overlay overlay {
        hexpand: true;
        vexpand: true;

        Stack content_stack {
          transition-type: crossfade;
          margin-start: 6;
          margin-end: 6;
          margin-top: 6;
          margin-bottom: 6;

          StackPage {
            name: "start";

            child: Button start {
              receives-default: true;
              clicked => $start_button_clicked_cb();

              Box {
                orientation: vertical;
                homogeneous: true;

                Label {
                  label: _("START");

                  styles [
                    "rotate180",
                  ]
                }

                Label {
                  valign: center;
                  label: _("START");
                  use-markup: true;
                }
              }

              styles [
                "suggested-action",
              ]
            };
          }
        }

        [overlay]
        Box {
          orientation: vertical;
          margin-start: 6;
          margin-end: 6;
          margin-top: 6;
          margin-bottom: 6;
          halign: start;

          ProgressBar progress1 {
            orientation: vertical;
            vexpand: true;

            styles [
              "rotate180",
            ]
          }

          ProgressBar progress2 {
            orientation: vertical;
            vexpand: true;
          }
        }
      }

      Button player_two_button {
        receives-default: true;
        margin-start: 6;
        margin-end: 6;
        margin-bottom: 6;
        clicked => $player_two_button_clicked_cb();

        Box {
          orientation: vertical;
          hexpand: true;
          vexpand: true;
          margin-start: 12;
          margin-end: 12;
          margin-top: 12;
          margin-bottom: 12;

          Label player_two_description {}

          [end]
          Label {
            label: _("Player 2");
            vexpand: true;
            valign: end;
            wrap: true;
          }
        }
      }
    }
  }
}
