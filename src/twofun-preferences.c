/* Copyright 2018-2023 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "twofun-preferences.h"

struct _TwoFunPreferences {
  AdwPreferencesDialog parent_instance;
};

G_DEFINE_TYPE (TwoFunPreferences, twofun_preferences, ADW_TYPE_PREFERENCES_DIALOG)

static void
twofun_preferences_class_init (TwoFunPreferencesClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/tabos/twofun/twofun-preferences.ui");
}

static void
twofun_preferences_init (TwoFunPreferences *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

