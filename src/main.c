/* Copyright 2018-2023 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "twofun-config.h"
#include "twofun-preferences.h"
#include "twofun-window.h"

TwoFunWindow *window;

static void
app_start (GSimpleAction *action,
           GVariant      *parameter,
           gpointer       user_data)
{
  twofun_window_start_game (window);
}

static void
app_about (GSimpleAction *action,
           GVariant      *parameter,
           gpointer       user_data)
{
  GtkApplication *app = GTK_APPLICATION (user_data);
  AdwDialog *dialog = adw_about_dialog_new_from_appdata ("/org/tabos/twofun/org.tabos.twofun.metainfo.xml", NULL);

  adw_dialog_present (dialog, GTK_WIDGET (gtk_application_get_active_window (app)));
}

static void
app_preferences (GSimpleAction *action,
                 GVariant      *parameter,
                 gpointer       user_data)
{
  GtkWidget *dialog;
  GtkApplication *app = GTK_APPLICATION (user_data);

  dialog = g_object_new (TWOFUN_TYPE_PREFERENCES, NULL);

  adw_dialog_present (ADW_DIALOG (dialog), GTK_WIDGET (gtk_application_get_active_window (app)));
}

static GActionEntry app_entries[] = {
  { "start", app_start, NULL, NULL, NULL },
  { "about", app_about, NULL, NULL, NULL },
  { "preferences", app_preferences, NULL, NULL, NULL },
};

static void
startup (GtkApplication *app)
{
  g_action_map_add_action_entries (G_ACTION_MAP (app),
                                   app_entries, G_N_ELEMENTS (app_entries),
                                   app);
}

static void
on_activate (GtkApplication *app)
{
  /* It's good practice to check your parameters at the beginning of the
   * function. It helps catch errors early and in development instead of
   * by your users.
   */
  g_assert (GTK_IS_APPLICATION (app));

  /* Get the current window or create one if necessary. */
  window = TWOFUN_WINDOW (gtk_application_get_active_window (app));
  if (window == NULL) {
    GtkCssProvider *css_provider;
    css_provider = gtk_css_provider_new ();
    gtk_css_provider_load_from_resource (css_provider, "/org/tabos/twofun/twofun.css");
    gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                    GTK_STYLE_PROVIDER (css_provider),
                                    GTK_STYLE_PROVIDER_PRIORITY_USER);
    window = g_object_new (TWOFUN_TYPE_WINDOW,
                           "application", app,
                           NULL);
  }

  /* Ask the window manager/compositor to present the window. */
  gtk_window_present (GTK_WINDOW (window));
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr (AdwApplication) app = NULL;
  int ret;

  /* Set up gettext translations */
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  /*
   * Create a new GtkApplication. The application manages our main loop,
   * application windows, integration with the window manager/compositor, and
   * desktop features such as file opening and single-instance applications.
   */
  app = adw_application_new ("org.tabos.twofun", G_APPLICATION_DEFAULT_FLAGS);
  g_set_application_name (_("TwoFun"));

  gtk_window_set_default_icon_name ("org.tabos.twofun");

  /*
   * We connect to the activate signal to create a window when the application
   * has been launched. Additionally, this signal notifies us when the user
   * tries to launch a "second instance" of the application. When they try
   * to do that, we'll just present any existing window.
   *
   * Because we can't pass a pointer to any function type, we have to cast
   * our "on_activate" function to a GCallback.
   */
  g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);

  g_signal_connect (app, "startup", G_CALLBACK (startup), NULL);

  /*
   * Run the application. This function will block until the application
   * exits. Upon return, we have our exit code to return to the shell. (This
   * is the code you see when you do `echo $?` after running a command in a
   * terminal.
   *
   * Since GtkApplication inherits from GApplication, we use the parent class
   * method "run". But we need to cast, which is what the "G_APPLICATION()"
   * macro does.
   */
  ret = g_application_run (G_APPLICATION (app), argc, argv);

  return ret;
}
