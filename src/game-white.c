/* Copyright 2018-2021 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "game-white.h"

#include <glib/gi18n.h>

typedef struct {
  GtkWidget *drawing_area;

  GRand *rand;
  gboolean valid;
} TwoFunGameWhitePrivate;

struct _TwoFunGameWhite {
  TwoFunGame parent_instance;

  /*< private >*/
  TwoFunGameWhitePrivate *priv;
};

G_DEFINE_TYPE_WITH_PRIVATE (TwoFunGameWhite, twofun_game_white, TWOFUN_TYPE_GAME)

static gboolean
show_white (gpointer user_data)
{
  TwoFunGameWhite *self = TWOFUN_GAME_WHITE (user_data);
  TwoFunGameWhitePrivate *priv = twofun_game_white_get_instance_private (self);

  priv->valid = TRUE;
  gtk_widget_queue_draw (priv->drawing_area);

  g_signal_emit_by_name (TWOFUN_GAME (user_data), "progress-activated", 1.0f);

  return G_SOURCE_REMOVE;
}

static void
drawing_area_draw_cb (GtkDrawingArea *widget,
                      cairo_t        *cr,
                      int             width,
                      int             height,
                      gpointer        user_data)
{
  GdkRGBA color;
  TwoFunGameWhite *self = TWOFUN_GAME_WHITE (user_data);
  TwoFunGameWhitePrivate *priv = twofun_game_white_get_instance_private (self);

  cairo_rectangle (cr, 0, 0, width, height);

  color.alpha = 1.0;
  color.red = priv->valid ? 1.0 : 0.0;
  color.green = priv->valid ? 1.0 : 0.0;
  color.blue = priv->valid ? 1.0 : 0.0;
  gdk_cairo_set_source_rgba (cr, (const GdkRGBA *)&color);

  cairo_fill (cr);
}

gchar *
twofun_game_white_get_description (TwoFunGame *game)
{
  return g_strdup (_("Hit when white"));
}

void
twofun_game_white_next_step (TwoFunGame *game)
{
  TwoFunGameWhitePrivate *priv = twofun_game_white_get_instance_private (TWOFUN_GAME_WHITE (game));
  gint32 timeout;

  priv->valid = FALSE;
  gtk_widget_queue_draw (priv->drawing_area);

  timeout = g_rand_int_range (priv->rand, 500, 3500);
  g_timeout_add (timeout, show_white, game);
}

static void
twofun_game_white_class_init (TwoFunGameWhiteClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  TwoFunGameClass *game_class = TWOFUN_GAME_CLASS (klass);

  game_class->get_description = twofun_game_white_get_description;
  game_class->next_step = twofun_game_white_next_step;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/tabos/twofun/game-white.ui");

  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameWhite, drawing_area);
}

static void
twofun_game_white_init (TwoFunGameWhite *self)
{
  TwoFunGameWhitePrivate *priv = twofun_game_white_get_instance_private (self);

  gtk_widget_init_template (GTK_WIDGET (self));
  gtk_drawing_area_set_draw_func (GTK_DRAWING_AREA (priv->drawing_area), drawing_area_draw_cb, self, NULL);

  priv->rand = g_rand_new ();
}

GtkWidget *
twofun_game_white_new (void)
{
  return g_object_new (TWOFUN_TYPE_GAME_WHITE,
                       NULL);
}
