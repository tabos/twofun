/* Copyright 2018-2021 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "game-threefigures.h"

#include <glib/gi18n.h>

typedef struct {
  GtkWidget *image1;
  GtkWidget *image2;
  GtkWidget *image3;
  GtkWidget *image4;
  GtkWidget *image5;
  GtkWidget *image6;

  gint map[6];
  GRand *rand;
  gboolean valid;
  gboolean fill;
} TwoFunGameThreeFiguresPrivate;

struct _TwoFunGameThreeFigures {
  TwoFunGame parent_instance;

  /*< private >*/
  TwoFunGameThreeFiguresPrivate *priv;
};

G_DEFINE_TYPE_WITH_PRIVATE (TwoFunGameThreeFigures, twofun_game_threefigures, TWOFUN_TYPE_GAME)

static gchar *images[] = {
  "document-edit-symbolic",
  "mark-location-symbolic",
  "camera-web-symbolic",
  "tv-symbolic",
  "face-monkey-symbolic",
  "airplane-mode-symbolic",
  "start-here-symbolic",
};

static gsize images_len = G_N_ELEMENTS (images);

gchar *
twofun_game_threefigures_get_description (TwoFunGame *game)
{
  return g_strdup (_("Three of a kind?"));
}

static void
set_image (TwoFunGameThreeFiguresPrivate *priv,
           gint                           pos)
{
  int idx = g_get_monotonic_time () % images_len;

  switch (pos) {
    case 0:
      gtk_image_set_from_icon_name (GTK_IMAGE (priv->image1), images[idx]);
      break;
    case 1:
      gtk_image_set_from_icon_name (GTK_IMAGE (priv->image2), images[idx]);
      break;
    case 2:
      gtk_image_set_from_icon_name (GTK_IMAGE (priv->image3), images[idx]);
      break;
    case 3:
      gtk_image_set_from_icon_name (GTK_IMAGE (priv->image4), images[idx]);
      break;
    case 4:
      gtk_image_set_from_icon_name (GTK_IMAGE (priv->image5), images[idx]);
      break;
    case 5:
      gtk_image_set_from_icon_name (GTK_IMAGE (priv->image6), images[idx]);
      break;
    default:
      return;
  }

  priv->map[pos] = idx;
}

static void
fill_grid (TwoFunGameThreeFiguresPrivate *priv)
{
  int idx;

  for (idx = 0; idx < 6; idx++)
    set_image (priv, idx);
}

void
twofun_game_threefigures_next_step (TwoFunGame *game)
{
  TwoFunGameThreeFiguresPrivate *priv = twofun_game_threefigures_get_instance_private (TWOFUN_GAME_THREEFIGURES (game));
  int pos = g_get_monotonic_time () % 6;

  if (!priv->fill) {
    set_image (priv, pos);
  } else {
    fill_grid (priv);
    priv->fill = FALSE;
  }

  g_signal_emit_by_name (TWOFUN_GAME (game), "progress-activated", 1.5f);
}

gboolean
twofun_game_threefigures_is_valid (TwoFunGame *game)
{
  TwoFunGameThreeFiguresPrivate *priv = twofun_game_threefigures_get_instance_private (TWOFUN_GAME_THREEFIGURES (game));
  gint count[10] = {0};
  gint idx;

  for (idx = 0; idx < 6; idx++) {
    count[priv->map[idx]]++;
  }

  priv->fill = TRUE;

  for (idx = 0; idx < 10; idx++) {
    if (count[idx] >= 3)
      return TRUE;
  }

  return FALSE;
}

static void
twofun_game_threefigures_class_init (TwoFunGameThreeFiguresClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  TwoFunGameClass *game_class = TWOFUN_GAME_CLASS (klass);

  game_class->get_description = twofun_game_threefigures_get_description;
  game_class->next_step = twofun_game_threefigures_next_step;
  game_class->is_valid = twofun_game_threefigures_is_valid;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/tabos/twofun/game-threefigures.ui");

  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameThreeFigures, image1);
  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameThreeFigures, image2);
  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameThreeFigures, image3);
  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameThreeFigures, image4);
  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameThreeFigures, image5);
  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameThreeFigures, image6);
}

static void
twofun_game_threefigures_init (TwoFunGameThreeFigures *self)
{
  TwoFunGameThreeFiguresPrivate *priv = twofun_game_threefigures_get_instance_private (self);

  gtk_widget_init_template (GTK_WIDGET (self));

  priv->rand = g_rand_new ();

  fill_grid (priv);
}

GtkWidget *
twofun_game_threefigures_new (void)
{
  return g_object_new (TWOFUN_TYPE_GAME_THREEFIGURES, NULL);
}
