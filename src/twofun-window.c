/* Copyright 2018-2023 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "game.h"
#include "twofun-config.h"
#include "twofun-window.h"

#include <cairo.h>
#include <glib/gi18n.h>

struct _TwoFunWindow {
  AdwApplicationWindow parent_instance;

  GSettings *settings;
  GtkHeaderBar *header_bar;

  GtkWidget *start_button;

  GtkWidget *overlay;
  GtkWidget *player_one_button;
  GtkWidget *player_one_description;
  GtkWidget *player_one_score;
  gint player_one_score_num;

  GtkWidget *player_two_button;
  GtkWidget *player_two_description;
  GtkWidget *player_two_score;
  gint player_two_score_num;

  GtkWidget *progress1;
  GtkWidget *progress2;
  GtkWidget *content_stack;

  GTimer *timer;
  gchar step;
  guint progress_source;
  gboolean clicked;
  gint clicks;

  gint active_game_nr;
  GtkWidget *active_game;
  gdouble game_time;
};

G_DEFINE_TYPE (TwoFunWindow, twofun_window, ADW_TYPE_APPLICATION_WINDOW)

extern GtkWidget *twofun_game_color_new (void);
extern GtkWidget *twofun_game_country_new (void);
extern GtkWidget *twofun_game_white_new (void);
extern GtkWidget *twofun_game_threefigures_new (void);

typedef struct {
  const gchar *name;
  GtkWidget *(*new)(void);
} TwoFunGameLoaderVtable;

static TwoFunGameLoaderVtable games[] = {
  {"white", twofun_game_white_new},
  {"color", twofun_game_color_new},
  {"country", twofun_game_country_new},
  {"threefigures", twofun_game_threefigures_new}
};

static gsize games_len = G_N_ELEMENTS (games);

GtkWidget *load_game(const gchar *name)
{
  int i;

  for (i = 0; i < games_len; i++) {
    if (!strcmp (games[i].name, name))
      return games[i].new ();
  }

  return NULL;
}

static gboolean progress_counter (gpointer self);

static
void set_progress (GObject *object,
                   gdouble  time,
                   gpointer user_data)
{
  TwoFunWindow *self = TWOFUN_WINDOW (user_data);

  g_clear_handle_id (&self->progress_source, g_source_remove);

  g_timer_reset (self->timer);
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (self->progress1), 1.0f);
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (self->progress2), 1.0f);

  self->game_time = time;
  self->progress_source = g_timeout_add (15, progress_counter, self);
}

static gint
number_of_steps (TwoFunWindow *self)
{
  switch (g_settings_get_uint (self->settings, "steps-per-game")) {
    case 0:
      return 2;
    case 1:
      return 4;
    case 2:
      return 8;
    case 3:
      return 16;
    default:
      break;
  }
  return 2;
}

static void
reset_game (TwoFunWindow *self)
{
  if (self->active_game) {
    g_signal_handlers_disconnect_by_func (self->active_game, set_progress, self);
  }
  g_clear_handle_id (&self->progress_source, g_source_remove);

  self->step = 0;
  self->active_game_nr = 0;
  self->clicks = 0;
  self->clicked = FALSE;
  self->player_two_score_num = 0;
  self->player_one_score_num = 0;
  gtk_widget_set_visible (self->player_one_score, FALSE);
  gtk_widget_set_visible (self->player_two_score, FALSE);
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (self->progress1), 0);
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (self->progress2), 0);
  gtk_label_set_text (GTK_LABEL (self->player_one_score), "0");
  gtk_label_set_text (GTK_LABEL (self->player_two_score), "0");

  gtk_stack_set_visible_child_name (GTK_STACK (self->content_stack), "start");
}

static void
set_button_color (TwoFunWindow *self,
                  int           player,
                  int           value)
{
  GtkWidget *button = player == 1 ? self->player_one_button : self->player_two_button;

  switch (value) {
  case 1:
    gtk_widget_add_css_class (button, "success");
    break;
  case 0:
    gtk_widget_add_css_class (button, "error");
    break;
  default:
    gtk_widget_remove_css_class (button, "success");
    gtk_widget_remove_css_class (button, "error");
    break;
  }
}

static gboolean
next_step (gpointer user_data)
{
  TwoFunWindow *self = TWOFUN_WINDOW (user_data);

  set_button_color (self, 1, -1);
  set_button_color (self, 2, -1);

  /* Last game is over? */
  if (self->clicks == number_of_steps (self)) {
    if (self->active_game_nr == games_len - 1) {
      g_autofree gchar *text = NULL;

      if (self->player_one_score_num > self->player_two_score_num) {
        text = g_strdup (_("Player 1 wins"));
        set_button_color (self, 1, 1);
      } else if (self->player_one_score_num < self->player_two_score_num) {
        text = g_strdup (_("Player 2 wins"));
        set_button_color (self, 2, 1);
      } else {
        text = g_strdup (_("It's a tie"));
      }

      gtk_label_set_text (GTK_LABEL (self->player_one_description), text);
      gtk_label_set_text (GTK_LABEL (self->player_two_description), text);
      reset_game (self);

      return G_SOURCE_REMOVE;
    }

    self->active_game_nr++;
    self->clicks = 0;
    self->step = 0;
  }

  /* Next game? */
  if (!self->step) {
    if (self->active_game) {
      g_signal_handlers_disconnect_by_func (self->active_game, set_progress, self);
    }
    gtk_stack_set_visible_child_name (GTK_STACK (self->content_stack), games[self->active_game_nr].name);
    self->active_game = gtk_stack_get_child_by_name (GTK_STACK (self->content_stack), games[self->active_game_nr].name);

    g_signal_connect (self->active_game, "progress-activated", G_CALLBACK (set_progress), self);
    g_autofree gchar *text = g_strdup (twofun_game_get_description (TWOFUN_GAME (self->active_game)));
    gtk_label_set_text (GTK_LABEL (self->player_one_description), text);
    gtk_label_set_text (GTK_LABEL (self->player_two_description), text);
  }

  self->clicked = FALSE;

  twofun_game_next_step (TWOFUN_GAME (self->active_game));

  self->step++;
  return G_SOURCE_REMOVE;
}

static gboolean
progress_counter (gpointer user_data)
{
  TwoFunWindow *self = TWOFUN_WINDOW (user_data);
  gdouble elapsed;
  gdouble fraction;

  elapsed = g_timer_elapsed (self->timer, NULL);

  fraction = (self->game_time - elapsed) / self->game_time;
  if (fraction <= 0.0f) {
    self->progress_source = 0;
    fraction = 0.0f;
    gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (self->progress1), fraction);
    gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (self->progress2), fraction);
    next_step (self);

    return G_SOURCE_REMOVE;
  }

  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (self->progress1), fraction);
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (self->progress2), fraction);

  return G_SOURCE_CONTINUE;
}

void
start_button_clicked_cb (GtkButton *button,
                         gpointer   user_data)
{
  TwoFunWindow *self = TWOFUN_WINDOW (user_data);

  gtk_widget_set_visible (self->player_one_score, TRUE);
  gtk_widget_set_visible (self->player_two_score, TRUE);
  next_step (self);
}

void
player_button_clicked (TwoFunWindow *self,
                       gint          player)
{
  g_autofree gchar *text = NULL;
  GtkProgressBar *progress;
  GtkWidget *label;
  gint score;
  gboolean active = !!self->progress_source;

  if (self->clicked || !self->active_game) {
    return;
  }

  self->clicks++;

  self->clicked = TRUE;

  switch (player) {
    case 1:
      progress = GTK_PROGRESS_BAR (self->progress1);
      score = self->player_one_score_num;
      label = self->player_one_score;
      break;
    case 2:
      progress = GTK_PROGRESS_BAR (self->progress2);
      score = self->player_two_score_num;
      label = self->player_two_score;
      break;
    default:
      return;
  }

  if (active && twofun_game_is_valid (TWOFUN_GAME (self->active_game))) {
    score++;
    set_button_color (self, player, 1);
  } else {
    score--;
    set_button_color (self, player, 0);
  }

  text = g_strdup_printf ("%d", score);
  gtk_label_set_text (GTK_LABEL (label), text);

  gtk_progress_bar_set_fraction (progress, 0.0f);

  switch (player) {
    case 1:
      self->player_one_score_num = score;
      break;
    case 2:
      self->player_two_score_num = score;
      break;
    default:
      break;
  }
}

void
player_one_button_clicked_cb (GtkButton *button,
                              gpointer   user_data)
{
  player_button_clicked (user_data, 1);
}

void
player_two_button_clicked_cb (GtkButton *button,
                              gpointer   user_data)
{
  player_button_clicked (user_data, 2);
}

static gboolean
on_delete_event (GtkWidget *widget,
                 GdkEvent  *event,
                 gpointer   user_data)
{
  TwoFunWindow *self = TWOFUN_WINDOW (widget);

  reset_game (self);

  return FALSE;
}

static void
twofun_window_dispose (GObject *object)
{
  TwoFunWindow *self = TWOFUN_WINDOW (object);

  g_clear_handle_id (&self->progress_source, g_source_remove);

  G_OBJECT_CLASS (twofun_window_parent_class)->dispose (object);
}

static void
twofun_window_class_init (TwoFunWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/tabos/twofun/twofun-window.ui");

  object_class->dispose = twofun_window_dispose;

  gtk_widget_class_bind_template_child (widget_class, TwoFunWindow, overlay);
  gtk_widget_class_bind_template_child (widget_class, TwoFunWindow, content_stack);
  gtk_widget_class_bind_template_child (widget_class, TwoFunWindow, player_one_button);
  gtk_widget_class_bind_template_child (widget_class, TwoFunWindow, player_two_button);
  gtk_widget_class_bind_template_child (widget_class, TwoFunWindow, player_one_description);
  gtk_widget_class_bind_template_child (widget_class, TwoFunWindow, player_two_description);
  gtk_widget_class_bind_template_child (widget_class, TwoFunWindow, progress1);
  gtk_widget_class_bind_template_child (widget_class, TwoFunWindow, progress2);
  gtk_widget_class_bind_template_callback (widget_class, start_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, player_one_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, player_two_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_delete_event);
}

static void
twofun_window_init (TwoFunWindow *self)
{
  PangoAttrList *attr_list;
  PangoAttribute *attr;
  int i;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = g_settings_new ("org.tabos.twofun");
  /* g_settings_bind (self->settings, "steps-per-game", self->steps_per_game, "active", G_SETTINGS_BIND_DEFAULT); */

  attr_list = pango_attr_list_new ();
  attr = pango_attr_scale_new (1.5);
  pango_attr_list_insert (attr_list, attr);

  self->player_one_score = gtk_label_new ("0");
  gtk_widget_add_css_class (self->player_one_score, "rotate180");
  gtk_label_set_attributes (GTK_LABEL (self->player_one_score), attr_list);
  gtk_widget_set_halign (self->player_one_score, GTK_ALIGN_END);
  gtk_widget_set_valign (self->player_one_score, GTK_ALIGN_START);
  gtk_widget_set_margin_start (self->player_one_score, 12);
  gtk_widget_set_margin_end (self->player_one_score, 12);
  gtk_widget_set_margin_top (self->player_one_score, 12);
  gtk_widget_set_margin_bottom (self->player_one_score, 12);
  gtk_overlay_add_overlay (GTK_OVERLAY (self->overlay), self->player_one_score);

  self->player_two_score = gtk_label_new ("0");
  gtk_label_set_attributes (GTK_LABEL (self->player_two_score), attr_list);
  gtk_widget_set_halign (self->player_two_score, GTK_ALIGN_END);
  gtk_widget_set_valign (self->player_two_score, GTK_ALIGN_END);
  gtk_widget_set_margin_start (self->player_two_score, 12);
  gtk_widget_set_margin_end (self->player_two_score, 12);
  gtk_widget_set_margin_top (self->player_two_score, 12);
  gtk_widget_set_margin_bottom (self->player_two_score, 12);
  gtk_overlay_add_overlay (GTK_OVERLAY (self->overlay), self->player_two_score);

  self->timer = g_timer_new ();

  /* Add games to internal structure */
  for (i = 0; i < games_len; i++) {
    GtkWidget *game;

    game = load_game (games[i].name);

    gtk_stack_add_named (GTK_STACK (self->content_stack), game, games[i].name);
  }
}

void twofun_window_start_game (TwoFunWindow *self)
{
  gtk_label_set_text (GTK_LABEL (self->player_one_description), "");
  gtk_label_set_text (GTK_LABEL (self->player_two_description), "");

  reset_game (self);
  self->active_game_nr = 0;
}

